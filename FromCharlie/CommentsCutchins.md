# Comments Cutchins

Include 6 files in your views directory:

```
$ ls -l
total 24
-rw-rw-r-- 1 charlie charlie  110 Feb 19 16:51 about.jade
-rw-rw-r-- 1 charlie charlie   84 Feb 19 15:30 error.jade
-rw-rw-r-- 1 charlie charlie  101 Feb 19 16:19 index.jade
-rw-rw-r-- 1 charlie charlie  636 Feb 19 17:05 layout.jade
-rw-rw-r-- 1 charlie charlie  710 Feb 19 16:18 menu-mixin.jade
-rw-rw-r-- 1 charlie charlie 3164 Mar 14 08:37 mixins.jade

```

Here is menu-mixin.jade:

```
//
   Created by charlie on 2/19/15.

extends layout

include mixins
    
block content
    body
        +nav("Prog272 Midterm", "dropdown_menu", "navbar-default navbar-fixed-top")
            +nav_item( "/", "home", "active" ) Home
            +nav_item( "json-ajax", "json-ajax") JsonAjax
            +nav_item( "about", "about" ) About
            +nav_item_dropdown( "#" )( label="Dropdown" )
                +nav_item( "#" ) Action
                +nav_item( "#" ) Another action
                +nav_item( "#" ) Something else here
                +nav_divider
                +nav_header Nav header
                +nav_item( "#" ) Separated link
                +nav_item( "#" ) One more separated link
```

And here is **index.jade**:

```
extends menu-mixin

block append content
  h1= title
  p Welcome to #{title}

  +attribution

  +bio
```

Do you see that **index.jade** extends **menu-mixin.jade** and 
**menu-mixin.jade** extends **layout.jade**?

I think I should also give you **layout.jade**, as yours is not
complete:

```
doctype html
html(lang='en')
  head
    meta(charset='UTF-8')
    meta(name='viewport', content='width=device-width')
    title= title
    block css
      link(rel='stylesheet', href='/components/bootstrap/dist/css/bootstrap.min.css')
      link(rel='stylesheet', href='/components/bootstrap/dist/css/bootstrap-theme.min.css')
      link(rel='stylesheet', href='/css/style.css')
    block js
      script(src='components/jquery/dist/jquery.js')
      script(src='components/bootstrap/dist/js/bootstrap.min.js')
      script(src='http://localhost:35729/livereload.js')
      script(src="javascripts/Control.js")
  body
    block content
```

It is important that you can put this kind of project together. As a result,
I'd like to submit it again.