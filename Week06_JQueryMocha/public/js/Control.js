/**
 * Created by bcuser on 2/12/15.
 */


$(document).ready(function () {

//////////////////////////   Male   ////////////////////////////////////////////////////////////////////////////////////
    $("#inputFirstName").val("");
    $("#MaleButton").click(function () {
        $.getJSON('Scientists.json', function (result) {
            console.log(result);
            for (var i = 0; i < result.length; i++) {
                var i = Math.floor(Math.random() * 5)
                if (i == 0) {
                    $("#inputFirstName").attr('placeholder', result[i].firstName);
                    $("#inputLastName").attr('placeholder', result[i].lastName);
                    $("#inputAddress").attr('placeholder', result[i].address);
                    $("#inputCity").attr('placeholder', result[i].city);
                    $("#inputState").attr('placeholder', result[i].state);

                }
                else if (i == 1) {
                    $("#inputFirstName").attr('placeholder', result[i].firstName);
                    $("#inputLastName").attr('placeholder', result[i].lastName);
                    $("#inputAddress").attr('placeholder', result[i].address);
                    $("#inputCity").attr('placeholder', result[i].city);
                    $("#inputState").attr('placeholder', result[i].state);

                }
                else if (i == 2) {
                    $("#inputFirstName").attr('placeholder', result[i].firstName);
                    $("#inputLastName").attr('placeholder', result[i].lastName);
                    $("#inputAddress").attr('placeholder', result[i].address);
                    $("#inputCity").attr('placeholder', result[i].city);
                    $("#inputState").attr('placeholder', result[i].state);

                }
                else {
                    $("#inputFirstName").attr('placeholder', result[i].firstName);
                    $("#inputLastName").attr('placeholder', result[i].lastName);
                    $("#inputAddress").attr('placeholder', result[i].address);
                    $("#inputCity").attr('placeholder', result[i].city);
                    $("#inputState").attr('placeholder', result[i].state);

                }

            }
        })

    });
///////////////////   Female   /////////////////////////////////////////////////////////////////////////////////////////
    $("#FemaleButton").click(function () {
        $.getJSON('Female.json', function (result) {
            console.log(result);
            for (var i = 0; i < result.length; i++) {
                var i = Math.floor(Math.random() * 5)
                if (i == 0) {
                    $("#inputFirstName").attr('placeholder', result[i].firstName);
                    $("#inputLastName").attr('placeholder', result[i].lastName);
                    $("#inputAddress").attr('placeholder', result[i].address);
                    $("#inputCity").attr('placeholder', result[i].city);
                    $("#inputState").attr('placeholder', result[i].state);

                }
                else if (i == 1) {
                    $("#inputFirstName").attr('placeholder', result[i].firstName);
                    $("#inputLastName").attr('placeholder', result[i].lastName);
                    $("#inputAddress").attr('placeholder', result[i].address);
                    $("#inputCity").attr('placeholder', result[i].city);
                    $("#inputState").attr('placeholder', result[i].state);

                }
                else if (i == 2) {
                    $("#inputFirstName").attr('placeholder', result[i].firstName);
                    $("#inputLastName").attr('placeholder', result[i].lastName);
                    $("#inputAddress").attr('placeholder', result[i].address);
                    $("#inputCity").attr('placeholder', result[i].city);
                    $("#inputState").attr('placeholder', result[i].state);

                }
                else {
                    $("#inputFirstName").attr('placeholder', result[i].firstName);
                    $("#inputLastName").attr('placeholder', result[i].lastName);
                    $("#inputAddress").attr('placeholder', result[i].address);
                    $("#inputCity").attr('placeholder', result[i].city);
                    $("#inputState").attr('placeholder', result[i].state);

                }

            }
        })

    });
/////////////////////////////////   Clear   ///////////////////////////////////////////////////////////////////////////

    $("#clearButton").click(function () {
        $("#inputFirstName").attr('placeholder', "First Name");
        $("#inputLastName").attr('placeholder', "Last Name");
        $("#inputAddress").attr('placeholder', "Address");
        $("#inputCity").attr('placeholder', "City");
        $("#inputState").attr('placeholder', "State");
    })


///////////////////////////////   Checkbox   //////////////////////////////////////////////////////////////////////////

///////////////////////////////   Male Checkbox   /////////////////////////////////////////////////////////////////////
    $("#insertMale").click(function () {
        $.getJSON('Scientists.json', function (result) {
            console.log(result);
            for (var i = 0; i < result.length; i++) {
                var i = Math.floor(Math.random() * 5)
                if (i == 0) {
                    $("#inputFirstName").attr('placeholder', result[i].firstName);
                    $("#inputLastName").attr('placeholder', result[i].lastName);
                    $("#inputAddress").attr('placeholder', result[i].address);
                    $("#inputCity").attr('placeholder', result[i].city);
                    $("#inputState").attr('placeholder', result[i].state);

                }
                else if (i == 1) {
                    $("#inputFirstName").attr('placeholder', result[i].firstName);
                    $("#inputLastName").attr('placeholder', result[i].lastName);
                    $("#inputAddress").attr('placeholder', result[i].address);
                    $("#inputCity").attr('placeholder', result[i].city);
                    $("#inputState").attr('placeholder', result[i].state);

                }
                else if (i == 2) {
                    $("#inputFirstName").attr('placeholder', result[i].firstName);
                    $("#inputLastName").attr('placeholder', result[i].lastName);
                    $("#inputAddress").attr('placeholder', result[i].address);
                    $("#inputCity").attr('placeholder', result[i].city);
                    $("#inputState").attr('placeholder', result[i].state);

                }
                else {
                    $("#inputFirstName").attr('placeholder', result[i].firstName);
                    $("#inputLastName").attr('placeholder', result[i].lastName);
                    $("#inputAddress").attr('placeholder', result[i].address);
                    $("#inputCity").attr('placeholder', result[i].city);
                    $("#inputState").attr('placeholder', result[i].state);

                }

            }
        })

    });
//////////////   Female Checkbox   ////////////////////////////////////////////////////////////////////////////////

    $("#insertFemale").click(function () {
        $.getJSON('Female.json', function (result) {
            console.log(result);
            for (var i = 0; i < result.length; i++) {
                var i = Math.floor(Math.random() * 5)
                if (i == 0) {
                    $("#inputFirstName").attr('placeholder', result[i].firstName);
                    $("#inputLastName").attr('placeholder', result[i].lastName);
                    $("#inputAddress").attr('placeholder', result[i].address);
                    $("#inputCity").attr('placeholder', result[i].city);
                    $("#inputState").attr('placeholder', result[i].state);

                }
                else if (i == 1) {
                    $("#inputFirstName").attr('placeholder', result[i].firstName);
                    $("#inputLastName").attr('placeholder', result[i].lastName);
                    $("#inputAddress").attr('placeholder', result[i].address);
                    $("#inputCity").attr('placeholder', result[i].city);
                    $("#inputState").attr('placeholder', result[i].state);

                }
                else if (i == 2) {
                    $("#inputFirstName").attr('placeholder', result[i].firstName);
                    $("#inputLastName").attr('placeholder', result[i].lastName);
                    $("#inputAddress").attr('placeholder', result[i].address);
                    $("#inputCity").attr('placeholder', result[i].city);
                    $("#inputState").attr('placeholder', result[i].state);

                }
                else {
                    $("#inputFirstName").attr('placeholder', result[i].firstName);
                    $("#inputLastName").attr('placeholder', result[i].lastName);
                    $("#inputAddress").attr('placeholder', result[i].address);
                    $("#inputCity").attr('placeholder', result[i].city);
                    $("#inputState").attr('placeholder', result[i].state);

                }

            }
        })

    });
////////////////////////////////////   Get Info   /////////////////////////////////////////////////////////////////////
////////////////////////////////////   Name   /////////////////////////////////////////////////////////////////////////
    $("#getName").click(function () {
        $.getJSON('Presidents.json', function (result) {
            console.log(result);
            for (var i = 0; i < result.length; i++) {
                var i = Math.floor(Math.random() * 5)
                if (i == 0) {
                    $("#putName").html(result[i].firstName + " " + result[i].lastName);
                }
                else if (i == 1) {
                    $("#putName").html(result[i].firstName + " " + result[i].lastName);
                }
                else if (i == 2) {
                    $("#putName").html(result[i].firstName + " " + result[i].lastName);
                }
                else {
                    $("#putName").html(result[i].firstName + " " + result[i].lastName);
                }
            }
        })
    });
///////////////////////////////////   City, State, Zip   ///////////////////////////////////////////////////////////////

    $("#getAddress").click(function () {
        $.getJSON('Presidents.json', function (result) {
            console.log(result);
            for (var i = 0; i < result.length; i++) {
                var i = Math.floor(Math.random() * 5)
                if (i == 0) {
                    $("#putAddress").html(result[i].city + " " + result[i].state + " " + result[i].zip);
                }
                else if (i == 1) {
                    $("#putAddress").html(result[i].city + " " + result[i].state + " " + result[i].zip);
                }
                else if (i == 2) {
                    $("#putAddress").html(result[i].city + " " + result[i].state + " " + result[i].zip);
                }
                else {
                    $("#putAddress").html(result[i].city + " " + result[i].state + " " + result[i].zip);
                }
            }
        })
    });
//////////////////////////////////   Age   ////////////////////////////////////////////////////////////////////////////

    $("#getAge").click(function () {
        $.getJSON('Presidents.json', function (result) {
            console.log(result);
            for (var i = 0; i < result.length; i++) {
                var i = Math.floor(Math.random() * 5)
                if (i == 0) {
                    $("#putAge").html(result[i].age);
                }
                else if (i == 1) {
                    $("#putAge").html(result[i].age);
                }
                else if (i == 2) {
                    $("#putAge").html(result[i].age);
                }
                else {
                    $("#putAge").html(result[i].age);
                }
            }
        })
    });

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    $("#getPresidents").click(function () {
        $.getJSON('Presidents.json', function (result) {
            console.log(result);

            for (var i = 0; i < result.length; i++) {
                $("#displayPresidents").append("<li>" + result[i].firstName + " " + result[i].lastName + "</li>")
            }

        });
    });
    $("#ajax").click(function () {
        $.getJSON('Presidents.json', function (result) {
            console.log(result);

            for (var i = 0; i < result.length; i++) {
                $("#displayAjax").append("<li>" + result[i].firstName + " " + result[i].lastName + "</li>")
            }

        });
    });

});


// $("#inputFirstName").val("Patrick");
// $("#firstNameButton").click(function () {
//    var firstName = $("#inputFirstName").val();
//    console.log(firstName);
//    $("#firstName").html(firstName)
//