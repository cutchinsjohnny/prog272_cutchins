/**
 * Created by bcuser on 2/23/15.
 */
var express = require('express');
var router = express.Router();

router.get('/', function(req, res) {
    res.render('About', {
        title: 'Prog272-Calvert',
        description: 'jquery demo' });
});

module.exports = router;