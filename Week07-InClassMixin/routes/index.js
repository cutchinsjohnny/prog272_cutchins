var express = require('express');
var router = express.Router();

/* GET home page. */

router.get('/', function(req, res) {
  res.render('index', { title: 'Week07' });
});
router.get('/read', function(request, response) {
    res.send({"result": "Success sent from routes/index.js."});
});

module.exports = router;
