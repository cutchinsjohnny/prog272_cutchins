/**
 * Created by bcuser on 2/12/15.
 */

$(document).ready(function () {
    $("#debug").html("Hello and Welcome!");

    $("#inputFirstName").val("");
    $("#firstNameButton").click(function () {
        var firstName = $("#inputFirstName").val();
        console.log(firstName);
        $("#firstName").html(firstName)
    });

    $("#inputLastName").val("");
    $("#lastNameButton").click(function () {
        var lastName = $("#inputLastName").val();
        console.log(lastName);
        $("#lastName").html(lastName)


    });

    $("#getPresidents").click(function() {
        $.getJSON('Presidents.json',function(result){
            console.log(result);

            for (var i = 0; i < result.length; i++) {
                $("#displayPresidents").append("<li>" + result[i].firstName + " " +result[i].lastName + "</li>")
            }

        });
    });



});