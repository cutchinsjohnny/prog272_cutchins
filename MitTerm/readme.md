# Awesome jQuery plugin

> The best jQuery plugin ever.


## Getting Started

Download the [production version][min] or the [development version][max].

[min]: https://raw.githubusercontent.com//jquery-mitterm/master/dist/jquery.mitterm.min.js
[max]: https://raw.githubusercontent.com//jquery-mitterm/master/dist/jquery.mitterm.js

In your web page:

```html
<script src="jquery.js"></script>
<script src="dist/mitterm.min.js"></script>
<script>
  jQuery(function ($) {
    $.awesome(); // "awesome"
  });
</script>
```


## License

MIT © 
